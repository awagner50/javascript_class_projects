# Interactive Front-end Development: Alex's student repository

This repository holds Java code that was created for my classes from CCAC. (Spring 2023)
This class mostly dealt with Javascript, but also used html and css.
All assignments and tests covered here will be from this class (MMC-150).

# Assignemnt 2: Intoduction to Javascript

This assignment is very basic. It contains a html and js file. I learn how to use alerts, prompts, operations, and more.

# Assignemnt 3: Strings and Arrays

This assignment contains a html and js file. It flips a string to read backwords and it also orders an array of strings in alphabetical order.

# Assignemnt 4: Strings and Recursion

This assignment contains a html and js file. It reverses a string and uses recursion on another section.

# Assignemnt 5: Lists

This assignment contains a html and js file. This assignment creates a lists, adds things to it, and deletes things from it.

# Assignemnt 6: Functionality and EventListener

This assignment contains a html and js file. There are buttons and text boxes for which a user can make their own todo list.

# Project 1: Fortune Picker

This project contains a html and js file. This project chooses a fortune for the user by having them pick a number 1 - 12.

# Project 2: Pizza Order

This assignment contains a html, css, and js file. Greates a pizza order.

# Project 3: BMI Calculator

This project contains a html and js file. Creates a BMI Calculator.

# Final Project: Pokémon Creator

This project contains a html and js file. Allows user to create their own pokemon. 
